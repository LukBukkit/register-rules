package tk.lukbukkit;

import org.bukkit.configuration.file.FileConfiguration;

public class GenConfig {

    public static void setupConfig(FileConfiguration config) {
        config.options().copyDefaults(true);
        config.addDefault("gruppe", "user");
        config.addDefault("anzahl_der_fragen", 5);
        for (int i = 1; i <= 5; i++) {
            config.addDefault("fragen." + i + ".frage", "Ist griefen erlaubt?");
            config.addDefault("fragen." + i + ".antwort1", "Ja sicher!");
            config.addDefault("fragen." + i + ".antwort2", "Sofern ich schon lange spiele, ja!");
            config.addDefault("fragen." + i + ".antwort3", "Nur nicht wenn Admins in der Nähe sind!");
            config.addDefault("fragen." + i + ".antwort4", "Es ist verboten!");
            config.addDefault("fragen." + i + ".richtige_antwort", 4);
        }
        
        config.options().copyHeader(true);
        config.options().header("RegisterRules von LukBukkit\nRegex: %a% = ae; %A% = AE; %o% = oe; %O% = OE; %u% = ue; %U% = UE; %s% = ss\nWichtig: Min. 5 Fragen");

    }

    public static void readConfig(FileConfiguration config) {
        int questions = config.getInt("anzahl_der_fragen");
        for (int i = 1; i <= questions; i++) {
            String question = replaceSpecChars(config.getString("fragen." + i + ".frage"));
            String answer1 = replaceSpecChars(config.getString("fragen." + i + ".antwort1"));
            String answer2 = replaceSpecChars(config.getString("fragen." + i + ".antwort2"));
            String answer3 = replaceSpecChars(config.getString("fragen." + i + ".antwort3"));
            String answer4 = replaceSpecChars(config.getString("fragen." + i + ".antwort4"));
            int correct = config.getInt("fragen." + i + ".richtige_antwort");
            RegisterRules.questions.add(new Question(question, answer1, answer2, answer3, answer4, correct));
        }
    }

    private static String replaceSpecChars(String s) {
        String r = s.replaceAll("%a%", "ä").replaceAll("%A%", "Ä").replaceAll("%o%", "ö").replaceAll("%O%", "ö").replaceAll("%u%", "ü")
                .replaceAll("%U%", "Ü").replaceAll("%s%", "ß");
        return r;
    }

}
