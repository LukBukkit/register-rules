package tk.lukbukkit.commands;

import java.util.ArrayList;
import java.util.Random;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import tk.lukbukkit.InventoryBuilder;
import tk.lukbukkit.Question;
import tk.lukbukkit.RegisterRules;
import static tk.lukbukkit.RegisterRules.currentinv;

public class RegisterCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (!(cs instanceof Player)) {
            cs.sendMessage(ChatColor.RED + "Du bist kein Spieler");
            return true;
        }
        Player p = (Player) cs;
        if(p.hasPermission("register.done")){
            p.sendMessage(ChatColor.GREEN + "Du bist bereits registriert!");
            return true;
        }
        RegisterRules.pquest.put(p, new ArrayList<>(RegisterRules.questions));
        RegisterRules.pright.put(p, 0);
        Random r = new Random();
        Question qu = RegisterRules.pquest.get(p).get(r.nextInt(RegisterRules.pquest.size()));
        RegisterRules.pquest.get(p).remove(qu);
        Inventory in = new InventoryBuilder(qu).build();
        RegisterRules.currentquestion.put(p, qu);
        currentinv.put(p, in);
        p.openInventory(in);
        return true;
    }

}
