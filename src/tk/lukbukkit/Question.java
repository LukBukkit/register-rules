package tk.lukbukkit;

public class Question {
    
    private String question;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private int correct;
    
    public Question(String question, String answer1, String answer2, String answer3, String answer4, int correct){
        this.question = question;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.correct = correct;
    }
    
    public String getQuestion(){
        return this.question;
    }
    
    public String getAnswer1(){
        return this.answer1;
    }
    
    public String getAnswer2(){
        return this.answer2;
    }
    
    public String getAnswer3(){
        return this.answer3;
    }
    
    public String getAnswer4(){
        return this.answer4;
    }
    
    public int getCorrect(){
        return this.correct;
    }
    
    public boolean checkCorrect(int answer){
        return (answer == this.correct);
    }

}
