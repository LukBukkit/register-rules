package tk.lukbukkit;

import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import tk.lukbukkit.commands.RegisterCMD;
import tk.lukbukkit.events.InventoryEvents;

public class RegisterRules extends JavaPlugin {
    
    public static ArrayList<Question> questions = new ArrayList<>();
    public static HashMap<Player, ArrayList<Question>> pquest = new HashMap<>();
    public static HashMap<Player, Integer> pright = new HashMap<>();
    public static HashMap<Player, Inventory> currentinv = new HashMap<>();
    public static HashMap<Player, Question> currentquestion = new HashMap<>();
    public static GroupManagerHook gmh;
    public static String usergroup;
    public static ArrayList<Player> closetrue = new ArrayList<>();

    @Override
    public void onDisable() {
        for(Player p : Bukkit.getOnlinePlayers()){
            Inventory in = p.getOpenInventory().getTopInventory();
            if(in != null && in.getTitle() != null && in.getTitle().startsWith(ChatColor.GREEN + "Frage: ")){
                p.closeInventory();
                p.sendMessage("Wegen eines Neuladens des Plugins wurde die Registrierung abgebrochen! Bitte wiederhole sie!");
            }
        }
    }

    @Override
    public void onEnable() {
        gmh = new GroupManagerHook(this);
        GenConfig.setupConfig(this.getConfig());
        this.saveConfig();
        GenConfig.readConfig(this.getConfig());
        usergroup = this.getConfig().getString("gruppe");
        //Comamands
        this.getCommand("register").setExecutor(new RegisterCMD());
        //Events
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new InventoryEvents(), this);
    }

}
