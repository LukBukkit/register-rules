package tk.lukbukkit;

import java.util.Arrays;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {

    private Material m;
    private int count;
    private short data;
    private String name;
    private List<String> lore;

    /**
     * Erzeugt einen neuen ItemBuilder
     *
     * @param m - Material des Items
     * @param count - Anzahl des Items
     */
    public ItemBuilder(Material m, int count) {
        this.m = m;
        this.count = count;
        this.data = (short) 0;
    }

    /**
     * Erzeugt einen neuen ItemBuilder
     *
     * @param m - Material des Items
     * @param count - Anzahl des Items
     * @param data - Blockabh�nige Daten
     */
    public ItemBuilder(Material m, int count, short data) {
        this.m = m;
        this.count = count;
        this.data = data;
    }

    /**
     * Setzt den Name des Items
     *
     * @param name - Name des Items
     */
    public ItemBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder setLore(String lore) {
        return setLore(new String[]{
            lore
        });
    }

    public ItemBuilder setLore(String[] lore) {
        return setLore(Arrays.asList(lore));
    }

    public ItemBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    /**
     * Erzeugt aus den Daten einen ItemStack
     *
     * @return ItemStack
     */
    public ItemStack build() {
        ItemStack is = new ItemStack(m, count, data);
        ItemMeta im = is.getItemMeta();
        if (name != null && !name.equalsIgnoreCase("") && !name.isEmpty()) {
            im.setDisplayName(name);
        }

        if (lore != null && lore.size() > 0 && !lore.isEmpty()) {
            im.setLore(lore);
        }
        is.setItemMeta(im);
        return is;
    }

}
