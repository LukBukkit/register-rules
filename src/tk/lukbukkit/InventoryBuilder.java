package tk.lukbukkit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

public class InventoryBuilder {

    Question qu;
    
    public InventoryBuilder(Question qu) {
        this.qu = qu;
    }

    public Inventory build() {
        String title = ChatColor.GREEN + "Frage: ";
        if(qu.getQuestion().length() > 23){
            title += qu.getQuestion().substring(0, 20) + "...";
        } else {
            title += qu.getQuestion();
        }
        Inventory in = Bukkit.createInventory(null, 9, title);
        in.setItem(0, new ItemBuilder(Material.SULPHUR, 1).setName(ChatColor.GREEN + "Frage:").setLore(ChatColor.GREEN + qu.getQuestion()).build());
        in.setItem(2, new ItemBuilder(Material.ENDER_PEARL, 1).setName(ChatColor.DARK_GREEN +qu.getAnswer1()).build());
        in.setItem(3, new ItemBuilder(Material.ENDER_PEARL, 1).setName(ChatColor.DARK_GREEN +qu.getAnswer2()).build());
        in.setItem(4, new ItemBuilder(Material.ENDER_PEARL, 1).setName(ChatColor.DARK_GREEN +qu.getAnswer3()).build());
        in.setItem(5, new ItemBuilder(Material.ENDER_PEARL, 1).setName(ChatColor.DARK_GREEN + qu.getAnswer4()).build());
        in.setItem(8, new ItemBuilder(Material.BARRIER, 1).setName(ChatColor.RED + "Abbrechen!").build());
        return in;
    }

}
