package tk.lukbukkit.events;

import java.util.Random;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import tk.lukbukkit.InventoryBuilder;
import tk.lukbukkit.Question;
import tk.lukbukkit.RegisterRules;
import static tk.lukbukkit.RegisterRules.currentinv;

public class InventoryEvents implements Listener {

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (e.getInventory() != null && e.getInventory().getTitle() != null && e.getInventory().getTitle().startsWith(ChatColor.GREEN + "Frage: ")) {
            Player p = (Player) e.getPlayer();
            p.sendMessage(ChatColor.RED + "Bitte setzte die Registrierung in naher Zeit fort!");
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getInventory() != null && e.getInventory().getTitle() != null && e.getInventory().getTitle().startsWith(ChatColor.GREEN + "Frage: ")) {
            ItemStack cis = e.getCurrentItem();
            if (cis != null && cis.getType() != null && !cis.getType().equals(Material.AIR)) {
                Player p = (Player) e.getWhoClicked();
                if (cis.getType().equals(Material.ENDER_PEARL)) {
                    int slot = e.getSlot();
                    if (RegisterRules.currentquestion.get(p).checkCorrect(slot - 1)) {
                        p.sendMessage(ChatColor.GREEN + "Du hast die richtige Antwort ausgewählt!");
                        RegisterRules.pright.put(p, RegisterRules.pright.get(p) + 1);
                        if (RegisterRules.pright.get(p) > 4) {
                            RegisterRules.closetrue.add(p);
                            p.closeInventory();
                            p.sendMessage(ChatColor.GREEN + "Herzlichen Glückwunsch! Du bist nun Member!");
                            p.playSound(p.getLocation(), Sound.FIREWORK_BLAST, 1, 1);
                            RegisterRules.gmh.setGroup(p, RegisterRules.usergroup);
                            RegisterRules.closetrue.remove(p);
                        } else {
                            Random r = new Random();
                            Question qu = RegisterRules.pquest.get(p).get(r.nextInt(RegisterRules.pquest.size()));
                            RegisterRules.pquest.get(p).remove(qu);
                            Inventory in = new InventoryBuilder(qu).build();
                            currentinv.put(p, in);
                            RegisterRules.closetrue.add(p);
                            p.closeInventory();
                            p.openInventory(in);
                            RegisterRules.currentquestion.put(p, qu);
                            RegisterRules.closetrue.remove(p);
                        }
                    } else {
                        RegisterRules.currentinv.remove(p);
                        RegisterRules.pquest.remove(p);
                        RegisterRules.pright.remove(p);
                        RegisterRules.currentquestion.remove(p);
                        RegisterRules.closetrue.add(p);
                        p.closeInventory();
                        RegisterRules.closetrue.remove(p);
                        p.sendMessage(ChatColor.RED + "Du hast die falsche Antwort ausgewählt! Du hast die Möglichkeit erneut von vorne zu beginnen!");
                    }
                } else if (cis.getType().equals(Material.BARRIER)) {
                    RegisterRules.currentinv.remove(p);
                    RegisterRules.pquest.remove(p);
                    RegisterRules.pright.remove(p);
                    RegisterRules.currentquestion.remove(p);
                    p.closeInventory();
                    p.sendMessage(ChatColor.RED + "Du hast den Vorgang abgebrochen, du kannst ihn später wiederholen!");
                }
            }
            e.setCancelled(true);
        }
    }

}
